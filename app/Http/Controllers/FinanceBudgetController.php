<?php

namespace App\Http\Controllers;

use App\Models\FinanceBudget;
use Illuminate\Http\Request;

/**
 * Class FinanceBudgetController
 * @package App\Http\Controllers
 */
class FinanceBudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financeBudgets = FinanceBudget::paginate();

        return view('finance-budget.index', compact('financeBudgets'))
            ->with('i', (request()->input('page', 1) - 1) * $financeBudgets->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeBudget = new FinanceBudget();
        return view('finance-budget.create', compact('financeBudget'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(FinanceBudget::$rules);

        $financeBudget = FinanceBudget::create($request->all());

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeBudget = FinanceBudget::find($id);

        return view('finance-budget.show', compact('financeBudget'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeBudget = FinanceBudget::find($id);

        return view('finance-budget.edit', compact('financeBudget'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceBudget $financeBudget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceBudget $financeBudget)
    {
        request()->validate(FinanceBudget::$rules);

        $financeBudget->update($request->all());

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $financeBudget = FinanceBudget::find($id)->delete();

        return redirect()->route('finance-budgets.index')
            ->with('success', 'FinanceBudget deleted successfully');
    }
}
