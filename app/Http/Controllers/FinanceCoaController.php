<?php

namespace App\Http\Controllers;

use App\Models\FinanceCoa;
use Illuminate\Http\Request;

/**
 * Class FinanceCoaController
 * @package App\Http\Controllers
 */
class FinanceCoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financeCoas = FinanceCoa::paginate();

        return view('finance-coa.index', compact('financeCoas'))
            ->with('i', (request()->input('page', 1) - 1) * $financeCoas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeCoa = new FinanceCoa();
        return view('finance-coa.create', compact('financeCoa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(FinanceCoa::$rules);

        $financeCoa = FinanceCoa::create($request->all());

        return redirect()->route('finance-coas.index')
            ->with('success', 'FinanceCoa created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeCoa = FinanceCoa::find($id);

        return view('finance-coa.show', compact('financeCoa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeCoa = FinanceCoa::find($id);

        return view('finance-coa.edit', compact('financeCoa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceCoa $financeCoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceCoa $financeCoa)
    {
        request()->validate(FinanceCoa::$rules);

        $financeCoa->update($request->all());

        return redirect()->route('finance-coas.index')
            ->with('success', 'FinanceCoa updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $financeCoa = FinanceCoa::find($id)->delete();

        return redirect()->route('finance-coas.index')
            ->with('success', 'FinanceCoa deleted successfully');
    }
}
