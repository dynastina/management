<?php

namespace App\Http\Controllers;

use App\Models\FinanceTransaction;
use Illuminate\Http\Request;

/**
 * Class FinanceTransactionController
 * @package App\Http\Controllers
 */
class FinanceTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $financeTransactions = FinanceTransaction::paginate();

        return view('finance-transaction.index', compact('financeTransactions'))
            ->with('i', (request()->input('page', 1) - 1) * $financeTransactions->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $financeTransaction = new FinanceTransaction();
        return view('finance-transaction.create', compact('financeTransaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(FinanceTransaction::$rules);

        $financeTransaction = FinanceTransaction::create($request->all());

        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $financeTransaction = FinanceTransaction::find($id);

        return view('finance-transaction.show', compact('financeTransaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $financeTransaction = FinanceTransaction::find($id);

        return view('finance-transaction.edit', compact('financeTransaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FinanceTransaction $financeTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinanceTransaction $financeTransaction)
    {
        request()->validate(FinanceTransaction::$rules);

        $financeTransaction->update($request->all());

        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $financeTransaction = FinanceTransaction::find($id)->delete();

        return redirect()->route('finance-transactions.index')
            ->with('success', 'FinanceTransaction deleted successfully');
    }
}
