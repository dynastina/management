<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employee
 *
 * @property $id
 * @property $image
 * @property $email
 * @property $position
 * @property $name
 * @property $place_of_birth
 * @property $date_of_birth
 * @property $phone
 * @property $address
 * @property $join_date
 * @property $remember_token
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Employee extends Model
{
    
    static $rules = [
		'image' => 'required',
		'email' => 'required',
		'position' => 'required',
		'name' => 'required',
		'place_of_birth' => 'required',
		'date_of_birth' => 'required',
		'phone' => 'required',
		'address' => 'required',
		'join_date' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['image','email','position','name','place_of_birth','date_of_birth','phone','address','join_date'];



}
