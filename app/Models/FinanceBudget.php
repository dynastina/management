<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FinanceBudget
 *
 * @property $id
 * @property $nomor_perkiraan
 * @property $nama_akun
 * @property $saldo
 * @property $saldo_sisa
 * @property $tahun
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class FinanceBudget extends Model
{
    
    static $rules = [
		'nomor_perkiraan' => 'required',
		'nama_akun' => 'required',
		'saldo' => 'required',
		'saldo_sisa' => 'required',
		'tahun' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nomor_perkiraan','nama_akun','saldo','saldo_sisa','tahun'];



}
