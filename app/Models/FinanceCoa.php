<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FinanceCoa
 *
 * @property $id
 * @property $nomor_perkiraan
 * @property $nama_akun
 * @property $header
 * @property $bagian
 * @property $status
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class FinanceCoa extends Model
{
    
    static $rules = [
		'nomor_perkiraan' => 'required',
		'nama_akun' => 'required',
		'header' => 'required',
		'bagian' => 'required',
		'status' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nomor_perkiraan','nama_akun','header','bagian','status'];



}
