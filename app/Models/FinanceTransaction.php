<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FinanceTransaction
 *
 * @property $id
 * @property $nomor_voucher
 * @property $total_debit
 * @property $total_kredit
 * @property $tanggal
 * @property $keterangan
 * @property $pembuat
 * @property $id_akun
 * @property $tipe_akun
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class FinanceTransaction extends Model
{
    
    static $rules = [
		'nomor_voucher' => 'required',
		'total_debit' => 'required',
		'total_kredit' => 'required',
		'tanggal' => 'required',
		'keterangan' => 'required',
		'pembuat' => 'required',
		'id_akun' => 'required',
		'tipe_akun' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nomor_voucher','total_debit','total_kredit','tanggal','keterangan','pembuat','id_akun','tipe_akun'];

    public function detail() {
      return $this->belongsTo('App\Models\FinanceTransactionDetail', 'id', 'finance_transaction_id');
  }



}
