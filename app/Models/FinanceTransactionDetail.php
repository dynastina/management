<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinanceTransactionDetail extends Model
{
    use HasFactory;

    static $rules = [
		'finance_transaction_id' => 'required',
		'nomor_perkiraan' => 'required',
		'nama_akun' => 'required',
		'remark' => 'required',
		'debit' => 'required',
		'kredit' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['finance_transaction_id','nomor_perkiraan','nama_akun','remark','debit','kredit'];
}
