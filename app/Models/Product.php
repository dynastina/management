<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property $id
 * @property $client_id
 * @property $image
 * @property $name
 * @property $description
 * @property $order_date
 * @property $order_finish_date
 * @property $status
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Product extends Model
{
    
    static $rules = [
		'client_id' => 'required',
		'image' => 'required',
		'name' => 'required',
		'description' => 'required',
		'order_date' => 'required',
		'order_finish_date' => 'required',
		'status' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id','image','name','description','order_date','order_finish_date','status'];



}
