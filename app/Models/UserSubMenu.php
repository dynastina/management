<?php

namespace App\Models;

use App\Models\UserMenu;
use App\Models\UserSubMenu;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserSubMenu extends Model
{
    use HasFactory;

    protected $fillable = [
        'menu_id',
        'name',
        'url',
        'order',
        'is_active',
    ];


    public function menu()
    {
        return $this->belongsTo(UserMenu::class, 'menu_id', 'id');
    }
}
