<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_voucher', '100');
            $table->decimal('total_debit', '15', '2');
            $table->decimal('total_kredit', '15', '2');
            $table->date('tanggal');
            $table->longText('keterangan');
            $table->string('pembuat', '100');
            $table->integer('id_akun');
            $table->string('tipe_akun', '25');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_transactions');
    }
}
