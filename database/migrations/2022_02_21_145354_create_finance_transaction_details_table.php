<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_transaction_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('finance_transaction_id');
            $table->string('nomor_perkiraan', '50');
            $table->string('nama_akun', '100');
            $table->longText('remark');
            $table->decimal('debit', '15', '2');
            $table->decimal('kredit', '15', '2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_transaction_details');
    }
}
