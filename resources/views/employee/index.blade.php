@extends('layouts.main')
@section('title', __('Employee'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('employees.index') }}" class="text-muted text-hover-primary">{{ __('Employee') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Employee') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<!--begin::Alert-->                                        
					
					<!--end::Alert-->
						<div class="table-responsive">
							<a href="{{ route('employees.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Employee') }} Baru</a>
							@if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
							<table class="table table-striped table-hover gy-7 gs-7 text-center">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										
										<th>Image</th>
										<th>Email</th>
										<th>Position</th>
										<th>Name</th>
										<th>Place Of Birth</th>
										<th>Date Of Birth</th>
										<th>Phone</th>
										<th>Address</th>
										<th>Join Date</th>

										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>                                                 
                                    @foreach ($employees as $employee)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $employee->image }}</td>
											<td>{{ $employee->email }}</td>
											<td>{{ $employee->position }}</td>
											<td>{{ $employee->name }}</td>
											<td>{{ $employee->place_of_birth }}</td>
											<td>{{ $employee->date_of_birth }}</td>
											<td>{{ $employee->phone }}</td>
											<td>{{ $employee->address }}</td>
											<td>{{ $employee->join_date }}</td>

                                            <td>
                                                <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('employees.show',$employee->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('employees.edit',$employee->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">


		$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

			var form = $(this).closest('form');

			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					form.submit();
				}
			})


		})

		

	</script>
@endsection