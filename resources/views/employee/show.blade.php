@extends('layouts.main')
@section('title', __('Employee'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('employees.index') }}" class="text-muted text-hover-primary">{{ __('Employee') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Employee') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Employee</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('employees.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Image:</strong>
                            {{ $employee->image }}
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $employee->email }}
                        </div>
                        <div class="form-group">
                            <strong>Position:</strong>
                            {{ $employee->position }}
                        </div>
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $employee->name }}
                        </div>
                        <div class="form-group">
                            <strong>Place Of Birth:</strong>
                            {{ $employee->place_of_birth }}
                        </div>
                        <div class="form-group">
                            <strong>Date Of Birth:</strong>
                            {{ $employee->date_of_birth }}
                        </div>
                        <div class="form-group">
                            <strong>Phone:</strong>
                            {{ $employee->phone }}
                        </div>
                        <div class="form-group">
                            <strong>Address:</strong>
                            {{ $employee->address }}
                        </div>
                        <div class="form-group">
                            <strong>Join Date:</strong>
                            {{ $employee->join_date }}
                        </div>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
