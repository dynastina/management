@extends('layouts.main')
@section('title', __('Finance Coa'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('finance-coas.index') }}" class="text-muted text-hover-primary">{{ __('Finance Coa') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Finance Coa') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Finance Coa</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('finance-coas.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nomor Perkiraan:</strong>
                            {{ $financeCoa->nomor_perkiraan }}
                        </div>
                        <div class="form-group">
                            <strong>Nama Akun:</strong>
                            {{ $financeCoa->nama_akun }}
                        </div>
                        <div class="form-group">
                            <strong>Header:</strong>
                            {{ $financeCoa->header }}
                        </div>
                        <div class="form-group">
                            <strong>Bagian:</strong>
                            {{ $financeCoa->bagian }}
                        </div>
                        <div class="form-group">
                            <strong>Status:</strong>
                            {{ $financeCoa->status }}
                        </div>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
