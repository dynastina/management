<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group mb-3">
            {{ Form::label('nomor_voucher') }}
            {{ Form::text('nomor_voucher', $financeTransaction->nomor_voucher, ['class' => 'form-control' . ($errors->has('nomor_voucher') ? ' is-invalid' : ''), 'placeholder' => 'Nomor Voucher']) }}
            {!! $errors->first('nomor_voucher', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('total_debit') }}
            {{ Form::text('total_debit', $financeTransaction->total_debit, ['class' => 'form-control' . ($errors->has('total_debit') ? ' is-invalid' : ''), 'placeholder' => 'Total Debit']) }}
            {!! $errors->first('total_debit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('total_kredit') }}
            {{ Form::text('total_kredit', $financeTransaction->total_kredit, ['class' => 'form-control' . ($errors->has('total_kredit') ? ' is-invalid' : ''), 'placeholder' => 'Total Kredit']) }}
            {!! $errors->first('total_kredit', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tanggal') }}
            {{ Form::text('tanggal', $financeTransaction->tanggal, ['class' => 'form-control' . ($errors->has('tanggal') ? ' is-invalid' : ''), 'placeholder' => 'Tanggal']) }}
            {!! $errors->first('tanggal', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('keterangan') }}
            {{ Form::text('keterangan', $financeTransaction->keterangan, ['class' => 'form-control' . ($errors->has('keterangan') ? ' is-invalid' : ''), 'placeholder' => 'Keterangan']) }}
            {!! $errors->first('keterangan', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('pembuat') }}
            {{ Form::text('pembuat', $financeTransaction->pembuat, ['class' => 'form-control' . ($errors->has('pembuat') ? ' is-invalid' : ''), 'placeholder' => 'Pembuat']) }}
            {!! $errors->first('pembuat', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('id_akun') }}
            {{ Form::text('id_akun', $financeTransaction->id_akun, ['class' => 'form-control' . ($errors->has('id_akun') ? ' is-invalid' : ''), 'placeholder' => 'Id Akun']) }}
            {!! $errors->first('id_akun', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group mb-3">
            {{ Form::label('tipe_akun') }}
            {{ Form::text('tipe_akun', $financeTransaction->tipe_akun, ['class' => 'form-control' . ($errors->has('tipe_akun') ? ' is-invalid' : ''), 'placeholder' => 'Tipe Akun']) }}
            {!! $errors->first('tipe_akun', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>