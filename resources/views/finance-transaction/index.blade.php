@extends('layouts.main')
@section('title', __('Finance Transaction'))
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('finance-transactions.index') }}" class="text-muted text-hover-primary">{{ __('Finance Transaction') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Finance Transaction') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<!--begin::Alert-->                                        
					
					<!--end::Alert-->
						<div class="table-responsive">
							<a href="{{ route('finance-transactions.create') }}" class="btn btn-primary mb-3" >Tambah {{ __('Finance Transaction') }} Baru</a>
							@if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
							<table class="table table-striped table-hover gy-7 gs-7 text-center">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										
										<th>Nomor Voucher</th>
										<th>Total Debit</th>
										<th>Total Kredit</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Pembuat</th>
										<th>Id Akun</th>
										<th>Tipe Akun</th>

										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>                                                 
                                    @foreach ($financeTransactions as $financeTransaction)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $financeTransaction->nomor_voucher }}</td>
											<td>{{ $financeTransaction->total_debit }}</td>
											<td>{{ $financeTransaction->total_kredit }}</td>
											<td>{{ $financeTransaction->tanggal }}</td>
											<td>{{ $financeTransaction->keterangan }}</td>
											<td>{{ $financeTransaction->pembuat }}</td>
											<td>{{ $financeTransaction->id_akun }}</td>
											<td>{{ $financeTransaction->tipe_akun }}</td>

                                            <td>
                                                <form action="{{ route('finance-transactions.destroy',$financeTransaction->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('finance-transactions.show',$financeTransaction->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('finance-transactions.edit',$financeTransaction->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">


		$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

			var form = $(this).closest('form');

			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					form.submit();
				}
			})


		})

		

	</script>
@endsection