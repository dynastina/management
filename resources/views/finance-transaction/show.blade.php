@extends('layouts.main')
@section('title', __('Finance Transaction'))
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ route('finance-transactions.index') }}" class="text-muted text-hover-primary">{{ __('Finance Transaction') }}</a>
						</li>
						<li class="breadcrumb-item text-dark">{{ __('Finance Transaction') }}</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
                <div class="card">
                    <div class="card-header">
                        <div class="float-left mt-4">
                            <span class="card-title">Show Finance Transaction</span>
                        </div>
                        <div class="float-right mt-4">
                            <a class="btn btn-primary" href="{{ route('finance-transactions.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nomor Voucher:</strong>
                            {{ $financeTransaction->nomor_voucher }}
                        </div>
                        <div class="form-group">
                            <strong>Total Debit:</strong>
                            {{ $financeTransaction->total_debit }}
                        </div>
                        <div class="form-group">
                            <strong>Total Kredit:</strong>
                            {{ $financeTransaction->total_kredit }}
                        </div>
                        <div class="form-group">
                            <strong>Tanggal:</strong>
                            {{ $financeTransaction->tanggal }}
                        </div>
                        <div class="form-group">
                            <strong>Keterangan:</strong>
                            {{ $financeTransaction->keterangan }}
                        </div>
                        <div class="form-group">
                            <strong>Pembuat:</strong>
                            {{ $financeTransaction->pembuat }}
                        </div>
                        <div class="form-group">
                            <strong>Id Akun:</strong>
                            {{ $financeTransaction->id_akun }}
                        </div>
                        <div class="form-group">
                            <strong>Tipe Akun:</strong>
                            {{ $financeTransaction->tipe_akun }}
                        </div>

                    </div>
                </div>
            <!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection
