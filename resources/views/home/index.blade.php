@extends('layouts.main')
@section('title', 'Home')
@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Content-->
        <div class="content fs-6 d-flex flex-column-fluid" id="kt_content">
            <!--begin::Container-->
            <div class="container">
                {{-- disini --}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex flex-column flex-column-fluid">
						<!--begin::Content-->
						<div class="content fs-6 d-flex flex-column-fluid" id="kt_content">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Row-->
								<div class="row g-0 g-xl-5 g-xxl-8">
									<div class="col-xl-4">
										<!--begin::Stats Widget 3-->
										<div class="card bg-danger card-stretch mb-5 mb-xxl-8">
											<!--begin::Body-->
											<div class="card-body">
												<!--begin::Section-->
												<div class="d-flex align-items-center">
													<!--begin::Symbol-->
													<div class="symbol symbol-50px me-5">
														<span class="symbol-label bg-white bg-opacity-10">
															<!--begin::Svg Icon | path: icons/duotone/Communication/Group-chat.svg-->
															<span class="svg-icon svg-icon-2x svg-icon-white">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" fill="#FFFFFF"><!--! Font Awesome Pro 6.0.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"/></svg>
															</span>
															<!--end::Svg Icon-->
														</span>
													</div>
													<!--end::Symbol-->
													<!--begin::Title-->
													<div>
														<a href="#" class="fs-4 text-white text-hover-primary fw-bolder">Jumlah Pegawai</a>
														<div class="fs-7 text-white opacity-75 fw-bold mt-1">4 Pegawai</div>
													</div>
													<!--end::Title-->
												</div>
												<!--end::Section-->
												<!--begin::Info-->
												<div class="fw-bolder text-white pt-7">
													<span class="d-block">3 Dev</span>
													<span class="d-block pt-2">1 Management</span>
												</div>
												<!--end::Info-->
												<!--begin::Progress-->
												<div class="progress h-6px mt-7 bg-white bg-opacity-10">
													<div class="progress-bar bg-white" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<!--end::Progress-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Stats Widget 3-->
									</div>
									<div class="col-xl-4">
										<!--begin::Stats Widget 4-->
										<div class="card card-stretch mb-5 mb-xxl-8">
											<!--begin::Body-->
											<div class="card-body">
												<!--begin::Section-->
												<div class="d-flex align-items-center">
													<!--begin::Symbol-->
													<div class="symbol symbol-50px me-5">
														<span class="symbol-label bg-light-success">
															<!--begin::Svg Icon | path: icons/duotone/Home/Library.svg-->
															<span class="svg-icon svg-icon-2x svg-icon-success">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" fill="#00B2FF"><!--! Font Awesome Pro 6.0.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M128 96h384v256h64V80C576 53.63 554.4 32 528 32h-416C85.63 32 64 53.63 64 80V352h64V96zM624 384h-608C7.25 384 0 391.3 0 400V416c0 35.25 28.75 64 64 64h512c35.25 0 64-28.75 64-64v-16C640 391.3 632.8 384 624 384zM365.9 286.2C369.8 290.1 374.9 292 380 292s10.23-1.938 14.14-5.844l48-48c7.812-7.813 7.812-20.5 0-28.31l-48-48c-7.812-7.813-20.47-7.813-28.28 0c-7.812 7.813-7.812 20.5 0 28.31l33.86 33.84l-33.86 33.84C358 265.7 358 278.4 365.9 286.2zM274.1 161.9c-7.812-7.813-20.47-7.813-28.28 0l-48 48c-7.812 7.813-7.812 20.5 0 28.31l48 48C249.8 290.1 254.9 292 260 292s10.23-1.938 14.14-5.844c7.812-7.813 7.812-20.5 0-28.31L240.3 224l33.86-33.84C281.1 182.4 281.1 169.7 274.1 161.9z"/></svg>
															</span>
															<!--end::Svg Icon-->
														</span>
													</div>
													<!--end::Symbol-->
													<!--begin::Title-->
													<div>
														<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder">Jumlah Aplikasi</a>
														<div class="fs-7 text-muted fw-bold mt-1">1 Aplikasi</div>
													</div>
													<!--end::Title-->
												</div>
												<!--end::Section-->
												<!--begin::Info-->
												<div class="fw-bolder text-muted pt-7">
													<span class="d-block">1 Aktif</span>
													<span class="d-block pt-2">0 Selesai</span>
												</div>
												<!--end::Info-->
												<!--begin::Progress-->
												<div class="progress h-6px bg-light-success mt-7">
													<div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<!--end::Progress-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Stats Widget 4-->
									</div>
									<div class="col-xl-4">
										<!--begin::Stats Widget 5-->
										<div class="card card-stretch mb-5 mb-xxl-8">
											<!--begin::Body-->
											<div class="card-body">
												<!--begin::Section-->
												<div class="d-flex align-items-center">
													<!--begin::Symbol-->
													<div class="symbol symbol-50px me-5">
														<span class="symbol-label bg-light-warning">
															<!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks.svg-->
															<span class="svg-icon svg-icon-2x svg-icon-warning">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" fill="#FFC700"><!--! Font Awesome Pro 6.0.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M319.9 320c57.41 0 103.1-46.56 103.1-104c0-57.44-46.54-104-103.1-104c-57.41 0-103.1 46.56-103.1 104C215.9 273.4 262.5 320 319.9 320zM369.9 352H270.1C191.6 352 128 411.7 128 485.3C128 500.1 140.7 512 156.4 512h327.2C499.3 512 512 500.1 512 485.3C512 411.7 448.4 352 369.9 352zM512 160c44.18 0 80-35.82 80-80S556.2 0 512 0c-44.18 0-80 35.82-80 80S467.8 160 512 160zM183.9 216c0-5.449 .9824-10.63 1.609-15.91C174.6 194.1 162.6 192 149.9 192H88.08C39.44 192 0 233.8 0 285.3C0 295.6 7.887 304 17.62 304h199.5C196.7 280.2 183.9 249.7 183.9 216zM128 160c44.18 0 80-35.82 80-80S172.2 0 128 0C83.82 0 48 35.82 48 80S83.82 160 128 160zM551.9 192h-61.84c-12.8 0-24.88 3.037-35.86 8.24C454.8 205.5 455.8 210.6 455.8 216c0 33.71-12.78 64.21-33.16 88h199.7C632.1 304 640 295.6 640 285.3C640 233.8 600.6 192 551.9 192z"/></svg>
															</span>
															<!--end::Svg Icon-->
														</span>
													</div>
													<!--end::Symbol-->
													<!--begin::Title-->
													<div>
														<a href="#" class="fs-4 text-gray-800 text-hover-primary fw-bolder">Jumlah Client</a>
														<div class="fs-7 text-muted fw-bold mt-1">1 Client</div>
													</div>
													<!--end::Title-->
												</div>
												<!--end::Section-->
												<!--begin::Info-->
												<div class="fw-bolder text-muted pt-7">
													<span class="d-block">1 Aktif</span>
													<span class="d-block pt-2">0 Selesai</span>
												</div>
												<!--end::Info-->
												<!--begin::Progress-->
												<div class="progress h-6px bg-light-warning mt-7">
													<div class="progress-bar bg-warning" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<!--end::Progress-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Stats Widget 5-->
									</div>
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row g-0 g-xl-5 g-xxl-8">
									<div class="col-xl-4">
										<!--begin::Stats Widget 1-->
										<div class="card card-stretch mb-5 mb-xxl-8">
											<!--begin::Header-->
											<div class="card-header align-items-center border-0 mt-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="fw-bolder text-dark fs-3">Nilai Kekayaan</span>
													<span class="text-muted mt-2 fw-bold fs-6">Rp. 900.000</span>
												</h3>
												<div class="card-toolbar">
													<!--begin::Dropdown-->
													<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
														<!--begin::Svg Icon | path: icons/duotone/Layout/Layout-4-blocks-2.svg-->
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
																	<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																</g>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</button>
													<!--begin::Menu-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
														<div class="menu-item px-3">
															<div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Manage</div>
														</div>
														<div class="separator mb-3 opacity-75"></div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Add User</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Add Role</a>
														</div>
														<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start" data-kt-menu-flip="left-start, top">
															<a href="#" class="menu-link px-3">
																<span class="menu-title">Add Group</span>
																<span class="menu-arrow"></span>
															</a>
															<div class="menu-sub menu-sub-dropdown w-200px py-4">
																<div class="menu-item px-3">
																	<a href="#" class="menu-link px-3">Admin Group</a>
																</div>
																<div class="menu-item px-3">
																	<a href="#" class="menu-link px-3">Staff Group</a>
																</div>
																<div class="menu-item px-3">
																	<a href="#" class="menu-link px-3">Member Group</a>
																</div>
															</div>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Reports</a>
														</div>
														<div class="separator mt-3 opacity-75"></div>
														<div class="menu-item px-3">
															<div class="menu-content px-3 py-3">
																<a class="btn btn-primary fw-bold btn-sm px-4" href="#">Create New</a>
															</div>
														</div>
													</div>
													<!--end::Menu-->
													<!--end::Dropdown-->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body pt-12">
												<!--begin::Chart-->
												<div class="d-flex flex-center position-relative bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-center h-175px" style="background-image:url('assets/media/svg/illustrations/bg-1.svg')">
													<div class="fw-bolder fs-1 text-gray-800 position-absolute">900K</div>
													<canvas id="kt_stats_widget_1_chart"></canvas>
												</div>
												<!--end::Chart-->
												<!--begin::Items-->
												<div class="d-flex justify-content-around pt-18">
													<!--begin::Item-->
													<div class="">
														<span class="fw-bolder text-gray-800">90% GDN</span>
														<span class="bg-info w-25px h-5px d-block rounded mt-1"></span>
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="">
														<span class="fw-bolder text-gray-800">10% RBN</span>
														<span class="bg-primary w-25px h-5px d-block rounded mt-1"></span>
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													{{-- <div class="">
														<span class="fw-bolder text-gray-800">32% SAP</span>
														<span class="bg-warning w-25px h-5px d-block rounded mt-1"></span>
													</div> --}}
													<!--end::Item-->
												</div>
												<!--end::Items-->
											</div>
											<!--end: Card Body-->
										</div>
										<!--end::Stats Widget 1-->
									</div>
									<div class="col-xl-8">
										<div class="card card-bordered">
                                            <div class="card-body">
                                                <div id="kt_apexcharts_3" style="height: 350px;"></div>
                                            </div>
                                        </div>
									</div>
								</div>
								<!--end::Row-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Content-->
					</div>
                    </div>
                </div>
                {{-- /disini --}}
            </div>
            <!--end::Container-->
        </div>
        <!--end::Content-->
    </div>
@endsection

@section('scripts')
    {{-- chart --}}
    <script type="text/javascript">
        // Class definition
        var KTWidgets = function () {
            // Stats widgets
            var initStatsWidget1 = function() {
                var element = document.querySelector("#kt_stats_widget_1_chart");

                if ( !element ) {
                }

                var randomScalingFactor = function() {
                    return Math.round(Math.random() * 100);
                };

                var tooltipBgColor = KTUtil.getCssVariableValue('--bs-gray-200');
                var tooltipColor = KTUtil.getCssVariableValue('--bs-gray-800');

                var color1 = KTUtil.getCssVariableValue('--bs-success');
                var color2 = KTUtil.getCssVariableValue('--bs-primary');
                var color3 = KTUtil.getCssVariableValue('--bs-warning');

                var config = {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: [810000, 9000],
                            backgroundColor: [color1, color2, color3]
                        }],
                        labels: ['GDN', 'RBN']
                    },
                    options: {
                        chart: {
                            fontFamily: 'inherit'
                        },
                        cutout: '75%',
                        cutoutPercentage: 75,
                        responsive: true,
                        maintainAspectRatio: false,
                        title: {
                            display: false,
                            text: 'Technology'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        tooltips: {
                            enabled: true,
                            intersect: false,
                            mode: 'nearest',
                            bodySpacing: 5,
                            yPadding: 10,
                            xPadding: 10,
                            caretPadding: 0,
                            displayColors: false,
                            backgroundColor: tooltipBgColor,
                            bodyFontColor: tooltipColor,
                            cornerRadius: 4,
                            footerSpacing: 0,
                            titleSpacing: 0
                        },
                        plugins: {
                            legend: {
                                display: false
                            }
                        }
                    }
                };

                var ctx = element.getContext('2d');
                var myDoughnut = new Chart(ctx, config);
            }

            // Public methods
            return {
                init: function () {
                    // Init Stats widgets
                    initStatsWidget1();

                    // Init Stats Widgets
                    initStatsWidget2('#kt_stats_widget_2_tab_1', '#kt_stats_widget_2_chart_1', [44, 55, 57, 56, 61, 58], [76, 85, 101, 98, 87, 105], true);
                    initStatsWidget2('#kt_stats_widget_2_tab_2', '#kt_stats_widget_2_chart_2', [35, 60, 35, 50, 45, 30], [65, 80, 50, 80, 75, 105], false);
                    initStatsWidget2('#kt_stats_widget_2_tab_3', '#kt_stats_widget_2_chart_3', [25, 40, 45, 50, 40, 60], [76, 85, 101, 98, 87, 105], false);
                    initStatsWidget2('#kt_stats_widget_2_tab_4', '#kt_stats_widget_2_chart_4', [50, 35, 45, 55, 30, 40], [76, 85, 101, 98, 87, 105], false);               

                    // Init Mixed Widgets
                    initMixedWidget1();
                    initMixedWidget2();

                    // Init Form Widgets
                    initFormWidget1();
                }
            }
        }();
    </script>
    <script type="text/javascript">

        var element = document.getElementById('kt_apexcharts_3');

        var height = parseInt(KTUtil.css(element, 'height'));
        var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
        var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
        var baseColor = KTUtil.getCssVariableValue('--bs-info');
        var lightColor = KTUtil.getCssVariableValue('--bs-light-info');

        if (!element) {
            
        }

        var options = {
            series: [{
                name: 'Nilai Kekayaan',
                data: [900000]
            }],
            chart: {
                fontFamily: 'inherit',
                type: 'area',
                height: height,
                toolbar: {
                    show: false
                }
            },
            plotOptions: {

            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'solid',
                opacity: 1
            },
            stroke: {
                curve: 'smooth',
                show: true,
                width: 3,
                colors: [baseColor]
            },
            xaxis: {
                categories: ['Feb'],
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: labelColor,
                        fontSize: '12px'
                    }
                },
                crosshairs: {
                    position: 'front',
                    stroke: {
                        color: baseColor,
                        width: 1,
                        dashArray: 3
                    }
                },
                tooltip: {
                    enabled: true,
                    formatter: undefined,
                    offsetY: 0,
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            yaxis: {
                labels: {
                    style: {
                        colors: labelColor,
                        fontSize: '12px'
                    }
                }
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px'
                },
                y: {
                    formatter: function (val) {
                        return 'Rp. ' + val + ''
                    }
                }
            },
            colors: [lightColor],
            grid: {
                borderColor: borderColor,
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            },
            markers: {
                strokeColor: baseColor,
                strokeWidth: 3
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();

    </script>
@endsection
