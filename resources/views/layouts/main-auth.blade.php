
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>{{ config('app.name') }}</title>
		<meta name="description" content="{{ config('app.name') }}" />
		<meta name="keywords" content="{{ config('app.name') }}" />
		<link rel="canonical" href="Https://preview.keenthemes.com/start" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{ URL::asset('assets/media/logos') }}/shield.png" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{ URL::asset('assets/plugins/global') }}/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ URL::asset('assets/css') }}/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

		<!--Begin::Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&amp;l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-5FS8GGP');</script>
		<!--End::Google Tag Manager -->

	</head>
	<!--end::Head-->

	<!--begin::Body-->
	<body id="kt_body" data-sidebar="on" class="bg-white header-fixed header-tablet-and-mobile-fixed toolbar-enabled sidebar-enabled">

		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
                <div class="d-flex flex-column flex-lg-row flex-column-fluid" id="kt_login">
                    <!--begin::Aside-->
                    <div class="d-flex flex-column flex-lg-row-auto bg-primary w-lg-600px pt-15 pt-lg-0">
                        <!--begin::Aside Top-->
                        <div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15 text-center">
                            <!--begin::Aside Logo-->
                            <a href="{{ url('') }}" class="mb-6">
                                <img alt="Logo" src="{{ URL::asset('assets/media/logos') }}/shield.png" class="h-75px" />
                            </a>
                            <!--end::Aside Logo-->
                            <!--begin::Aside Subtitle-->
                            <h3 class="fw-bolder fs-2x text-white lh-lg">Aplikasi Keuangan <br>Kelola Keuangan Perusahaan</h3>
                            <!--end::Aside Subtitle-->
                        </div>
                        <!--end::Aside Top-->
                        <!--begin::Aside Bottom-->
                        <div class="d-flex flex-row-fluid bgi-size-contain bgi-no-repeat bgi-position-y-bottom bgi-position-x-center min-h-350px" style="background-image: url({{ URL::asset('assets/media/illustrations') }}/statistics.png)"></div>
                        <!--end::Aside Bottom-->
                    </div>
                    <!--begin::Aside-->
			        @yield('content')
                </div>
			<!--end::Login-->
		</div>
		<!--end::Main-->

		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{ URL::asset('assets/plugins/global') }}/plugins.bundle.js"></script>
		<script src="{{ URL::asset('assets/js') }}/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="{{ URL::asset('assets/js') }}/custom/general/login.js"></script>
		<script src="{{ URL::asset('assets/js') }}/custom/apps/chat/chat.js"></script>
		<script src="{{ URL::asset('assets/js') }}/custom/intro.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->

		<!--Begin::Google Tag Manager (noscript) -->
		<noscript>
			<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<!--End::Google Tag Manager (noscript) -->

        {{-- includeable scripts --}}
		@yield('scripts')
	</body>
	<!--end::Body-->

</html>