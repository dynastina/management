@extends('layouts.main')
@section('title', 'Role Management')
@section('content')
<!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					<!--begin::Title-->
					<h3 class="text-dark fw-bolder my-1">Role</h3>
					<!--end::Title-->
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('') }}" class="text-muted text-hover-primary">Home</a>
						</li>
						<li class="breadcrumb-item">
							<a href="{{ url('role') }}" class="text-muted text-hover-primary">Role</a>
						</li>
						<li class="breadcrumb-item text-dark">Create</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<form action="" method="post" class="form-group">
							@csrf
							<div class="form-group mb-5">
								<label for="role_id">Role</label>
								<input type="text" class="form-control" id="role_id" aria-describedby="role_id" placeholder="Masukan Role" name="role_id" value="{{ $role->name }}" readonly>
							</div>
							<div class="form-group mb-5">
								<label for="role_id">Access Menu</label>
                                @foreach ($menu as $mn)
                                    <div class="form-check form-check-custom form-check-solid mb-3 mt-2">
										@php
											$checked = '';
										@endphp
										@foreach ($mn->access as $access)
											@if($access->role_id == $role->id) 
												@php
													$checked = 'checked="checked"';
												@endphp
											@endif											
										@endforeach
                                        <input class="form-check-input" type="checkbox" id="check{{ $loop->iteration }}" name="access[]" value="{{ $mn->id}}" {{ $checked }}/>
                                        <label class="form-check-label" for="check{{ $loop->iteration }}">
                                            {{ $mn->name }}
                                        </label>
                                    </div>
                                @endforeach
							</div>
							{{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
							<button type="button" class="btn btn-warning" onclick="history.back()">Back</button>
						</form>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
<!--end::Main-->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
        });

        $('.form-check-input').on('click', function(){
            var menu_id = $(this).val();
            var role_id = `{{ $role->id }}`;

            $.ajax({
                url: `{{ route('api.accessProcess') }}`,
                data: {menu_id: menu_id, role_id:role_id},
                success: function(response) {
                    $( ".sidebar-main" ).load(window.location.href + " .sidebar-main" );
                }
            });

        });

    </script>
@endsection