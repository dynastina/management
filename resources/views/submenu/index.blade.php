@extends('layouts.main')
@section('title', 'Submenu Management')
@section('content')
    <!--begin::Main-->
	<div class="d-flex flex-column flex-column-fluid">
		<!--begin::toolbar-->
		<div class="toolbar" id="kt_toolbar">
			<div class="container d-flex flex-stack flex-wrap flex-sm-nowrap">
				<!--begin::Info-->
				<div class="d-flex flex-column align-items-start justify-content-center flex-wrap me-1">
					
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-line bg-transparent text-muted fw-bold p-0 my-1 fs-7">
						<li class="breadcrumb-item">
							<a href="{{ url('/submenu') }}" class="text-muted text-hover-primary">Submenu</a>
						</li>
						<li class="breadcrumb-item text-dark">Submenu Management</li>							
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Info-->
				
				
			</div>
		</div>
		<!--end::toolbar-->
		<!--begin::Content-->
		<div class="content fs-6 d-flex flex-column-fluid mt-5" id="kt_content">
			<!--begin::Container-->
			<div class="container">
				<!--begin::Profile Account-->
				<div class="card" >
					<div class="card-body">
						<!--begin::Alert-->                                        
					
					<!--end::Alert-->
						<div class="table-responsive">
							<a href="{{ url('/submenu/create') }}" class="btn btn-primary mb-3" >Tambah Submenu Baru</a>
							@if (session('status'))
								<div class="alert alert-primary">
									{{ session('status') }}
								</div>
							@endif
							<table class="table table-striped table-hover gy-7 gs-7 text-center">
								<thead>
									<tr class="fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200">
										<th>No</th>
										<th>Menu</th>
										<th>Submenu</th>
										<th>Aksi</th>
										
									</tr>
								</thead>
								<tbody>                                                 
									@foreach($submenu as $sm)
										<tr>
											<td>{{ $loop->iteration }}</td>
											<td>{{ $sm->menu->name }}</td>
											<td>{{ $sm->name }}</td>
											<td>
												<form action="{{ url('submenu'. '/' .$sm->id) }}" class="form-group" method="post">
													@csrf
													@method('delete')
													<a href="{{ 'submenu/'. $sm->id . '/edit' }}" class="badge badge-primary">Edit</a>
													<button type="submit" class="badge badge-danger btn-delete" style="border:none">Delete</button>
												</form>
											</td>          
										</tr>       
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					</div>
				<!--end::Profile Account-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Content-->
	</div>
	<!--end::Main-->
@endsection

@section('scripts')
	<script type="text/javascript">


		$('body').on('click', '.btn-delete', function(event) {
			event.preventDefault();

			var form = $(this).closest('form');

			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					form.submit();
				}
			})


		})

		

	</script>
@endsection