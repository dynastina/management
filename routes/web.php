<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SubmenuController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\UserMenuController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\FinanceCoaController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\UserSubMenuController;
use App\Http\Controllers\FinanceBudgetController;
use App\Http\Controllers\FinanceTransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// buatan
Route::get('/role/{role}/access', [UserRoleController::class, 'access']);
Route::post('profile/reset/{id}', [UserProfileController::class, 'resetProfile']);
Route::get('profile/change-password', [UserProfileController::class, 'changePassword'])->name('change-password.index');
Route::patch('profile/change-password/{id}', [UserProfileController::class, 'changeProccess'])->name('profile.change-password');

// // resource
Route::resource('user', UserController::class);
Route::resource('menu', UserMenuController::class);
Route::resource('submenu', UserSubMenuController::class);
Route::resource('role', UserRoleController::class);
Route::resource('profile', UserProfileController::class);
Route::resource('employees', EmployeeController::class);
Route::resource('products', ProductController::class);
Route::resource('clients', ClientController::class);
Route::resource('finance-coas', FinanceCoaController::class);
Route::resource('finance-budgets', FinanceBudgetController::class);
Route::resource('finance-transactions', FinanceTransactionController::class);